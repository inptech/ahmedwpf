﻿using Pobric.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pobric.ViewModel
{
    public interface IRssHelper
    {
        List<Item> GetPosts();
    }
}
